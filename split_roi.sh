for i in $(find /Users/naomihunsaker/Imaging/hippocampus -type f -name "output.nii.gz"); do
    val=$(fslval $i dim1)    
    xsize=$(echo "$val/2" | bc)
    fslroi $i $(dirname $i)/HPCl.nii.gz 0 $xsize 0 -1 0 -1
    xmin=$xsize; xsize=$(echo "$val-$xmin" | bc)
    fslroi $i $(dirname $i)/HPCr.nii.gz $xmin $xsize 0 -1 0 -1
    /Applications/c3d/bin/c3d $(dirname $i)/HPCl.nii.gz -binarize -o $(dirname $i)/HPCl.nii.gz
    /Applications/c3d/bin/c3d $(dirname $i)/HPCr.nii.gz -binarize -o $(dirname $i)/HPCr.nii.gz
    fslmaths $(dirname $i)/HPCr.nii.gz -mul 2 $(dirname $i)/HPCr.nii.gz
    fslmerge -x $(dirname $i)/HPC.nii.gz $(dirname $i)/HPCr.nii.gz $(dirname $i)/HPCl.nii.gz
    rm $(dirname $i)/output.nii.gz
    rm $(dirname $i)/HPCr.nii.gz
    rm $(dirname $i)/HPCl.nii.gz
done 