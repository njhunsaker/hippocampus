#!/bin/sh

module load gcc segadapter

IFS=" "
base_dir=/home/njhunsak/projects/hippocampus/*
array=($base_dir)
for i in {0..9}; do
    HIPP=${array[$i]}/HPC_edited.nii.gz
    if [ ! -f $HIPP ]; then
        sa ${array[$i]}/brain_resliced_1x1x1.nii.gz ${array[$i]}/output_hipp.nii.gz /home/njhunsak/projects/segadapter/results/Cycle2/test1 ${array[$i]}/output.nii.gz
    fi
done