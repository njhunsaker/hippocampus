#!/bin/sh

# echo ${#array[@]}

module load gcc ants

IFS=" "
base_dir=/home/njhunsak/projects/hippocampus/*
array=($base_dir)
for i in {0..9}; do
  cd ${array[$i]}
  HIPP=HPC_edited.nii.gz
  if [ ! -f $HIPP ]; then
    FIX=/home/njhunsak/projects/hippocampus/template.nii.gz
    FIXH=/home/njhunsak/projects/hippocampus/template_roi.nii.gz
    FIXHIP=/home/njhunsak/projects/hippocampus/template_hippocampus.nii.gz
    MOV=brain_resliced_1x1x1.nii.gz
    MOVH=brain_resliced_1x1x1_roi.nii.gz
    OUT=output_
    if [ -f $MOVH ]; then
      ITS=100x100x100x20
      DIM=3
      LMWT=0.9
      INTWT=4
      PCT=0.8
      PARZ=100
      LM=PSE[${FIX},${MOV},$FIXH,$MOVH,${LMWT},${PCT},${PARZ},0,25,10000]
      INTENSITY=CC[$FIX,${MOV},${INTWT},4]
      ANTS $DIM -o $OUT -i $ITS -t SyN[0.1] -r Gauss[3,0.] -m $INTENSITY -m $LM
      WarpImageMultiTransform $DIM $FIXHIP ${OUT}hipp.nii.gz -i ${OUT}Affine.txt ${OUT}InverseWarp.nii.gz -R $MOV
    fi
  fi
done